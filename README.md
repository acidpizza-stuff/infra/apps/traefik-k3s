# Traefik k3s

[[_TOC_]]


## Enabling Metrics

https://github.com/cablespaghetti/k3s-monitoring/issues/9


## Traefik Basic Auth

### Create Password

```bash
printf "admin:$(openssl passwd -apr1)\n" > pass.txt
# key in a password
kubectl --namespace kube-system create secret generic traefik-auth --from-file=users=./pass.txt
```


## Traefik Ingress Class

Ingress Class must be enabled to use `spec.ingressClassName` in ingress resource. If ingress class is not enabled, we must use `metadata.annotations.kubernetes.io/ingress.class = traefik`.

Reference: https://github.com/k3s-io/k3s/issues/5562#issuecomment-1126311043
